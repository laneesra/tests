"use strict";

var assert = require('assert'),
test = require('selenium-webdriver/testing'),
webdriver = require('selenium-webdriver');
var driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
var i=0;

function clickLink(link) {
	link.click();
}

function findItem() {
	return driver.findElements(webdriver.By.className('lettersli')).then(function(result) {
		return result[i++];
	});
}

test.describe('Input yakutianLetters', function() {
	driver.get('http://ykt.ru/');
	var count=0;
	while (count!=5){
		driver.wait(findItem, 100).then(clickLink);
		count++;
	}
  test.it('should work', function() {
		var input = driver.findElement(webdriver.By.id('q-input'));
		input.getAttribute('value').then(function(value) {
				assert.equal(value, 'һөҕүҥ');
		});
		driver.quit();
  });
});
