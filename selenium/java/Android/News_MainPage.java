import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.junit.*;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {
    private static WebDriver driver;
    private static WebDriverWait wait;


    @BeforeClass
    public static void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities=DesiredCapabilities.android();

        capabilities.setCapability("noReset", "true");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.CHROME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"Android Emulator");
        capabilities.setCapability(MobileCapabilityType.VERSION,"6.0");

        URL url= new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AndroidDriver(url, capabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://news.ykt.ru/");
        wait = new WebDriverWait(driver, 10);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    private void makeScreenshot() throws Exception {
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("screen.png"));
    }

    @Test
    public void categoryMenu() throws Exception{
        List<WebElement> items = driver.findElements(By.cssSelector(".n-menu_item"));
        assertEquals("Количество категорий новостей", 3, items.size());
    }

    @Test
    public void newsPosts() throws Exception {
        try {
            WebElement main = driver.findElement(By.cssSelector(".n-m-main-post"));
        }
        catch (Exception e){
            makeScreenshot();
            throw new Exception("Главная новость не загружена");
        }
        List<WebElement> second = driver.findElements(By.cssSelector(".n-m-second-post"));
        assertEquals("Количество новостей", 2, second.size());
    }


    private String getText(WebElement item) throws Exception {
        try {
            ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", item);
        }
        catch (Exception e) {
            http://doska.ykt.ru/post-add
            throw new Exception("Невозможно проскролить поле");
        }
        return item.getText();
    }

    @Test
    public void dnevnikiPosts() throws Exception {
        List<WebElement> items = driver.findElements(By.cssSelector(".n-m-dnevniki_post_wrap"));
        assertTrue("Повторение полей в 'Блогеры'", getText(items.get(0))!=getText(items.get(1)) && getText(items.get(2))!=getText(items.get(1)) && getText(items.get(2))!=getText(items.get(0)));
    }

    @Test
    public void popularPosts() throws Exception {
        List<WebElement> items = driver.findElements(By.cssSelector(".yui-block_header"));
        assertTrue("Повторение полей в 'Популярное'", getText(items.get(0))!=getText(items.get(1)) && getText(items.get(2))!=getText(items.get(1)) && getText(items.get(2))!=getText(items.get(0)));
    }

    @Test
    public void latestNews() throws Exception {
        List<WebElement> news = driver.findElements(By.cssSelector(".n-latest-news_item"));
        Assert.assertEquals(15, news.size());
        WebElement more = driver.findElement(By.cssSelector(".yui-btn.yui-btn--news.yui-btn--block"));
        more.click();
        news = driver.findElements(By.cssSelector(".n-latest-news_item"));
        Assert.assertEquals(30, news.size());
    }

    @Test
    public void search() throws Exception{
        WebElement button = driver.findElement(By.cssSelector(".yui-btn.n-btn--toggle"));
        WebElement search = driver.findElement(By.id("js-n-m-search"));
        assertEquals("Поле поиска не скрыто", "n-m-search", search.getAttribute("class"));

        button.click();
        assertEquals("Поле поиска скрыто", "n-m-search js-n-m-search--show", search.getAttribute("class"));

        WebElement field = driver.findElement(By.name("q"));
        field.sendKeys("дом");
        button = driver.findElement(By.cssSelector(".yui-btn.n-btn--search"));
        button.click();
        field = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("q")));
        assertEquals("Значение в поле поиска неверно", "дом", field.getAttribute("value"));

        driver.get("http://news.ykt.ru/");
    }
}