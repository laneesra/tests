import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BuyTicket {
    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities=DesiredCapabilities.android();

        capabilities.setCapability("noReset", "true");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME,BrowserType.CHROME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM,Platform.ANDROID);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"Android Emulator");
        capabilities.setCapability(MobileCapabilityType.VERSION,"6.0");

        URL url= new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AndroidDriver(url, capabilities);
        //Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://afisha.ykt.ru/events/cinema");
    }

    @After
    public void tearDown() throws Exception {
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("screen.png"));
        driver.quit();
    }

    @Test
    public void testMethod() throws Exception{
        WebElement film = driver.findElement(By.cssSelector(".a-events_item_body"));
        film.click();
        WebElement buy = driver.findElement(By.cssSelector(".yui-btn.green"));
        buy.click();
        firstStep();
        secondStep();
        thirdStep();
    }

    private void firstStep() {
        List<WebElement> times = driver.findElements(By.className("a-seances_times_item"));
        for (WebElement t : times) {
            if (t.getAttribute("class").equals("a-seances_times_item")) {
                t.click();
                break;
            }
        }
    }

    private void secondStep() throws Exception{
        chooseSeat();
        clickContinue();
    }

    private void chooseSeat() throws Exception{
        try {
            WebElement seat = driver.findElement(By.cssSelector(".a-hall_row_seat.can-book.default"));
            seat.click();
        }
        catch (Exception e){
            throw new Exception("Нет свободных мест");
        }
    }

    private void clickContinue() throws Exception{
        try {
            WebElement next = driver.findElement(By.cssSelector(".yui-btn.green"));
            next.click();
        }
        catch (Exception e){
            throw new Exception("Кнопка 'Продолжить' недоступна");
        }
    }
}