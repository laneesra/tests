import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SearchHints {
    private WebDriver driver;

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities=DesiredCapabilities.android();

        capabilities.setCapability("noReset", "true");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME,BrowserType.CHROME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM,Platform.ANDROID);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"Android Emulator");
        capabilities.setCapability(MobileCapabilityType.VERSION,"6.0");

        URL url= new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AndroidDriver(url, capabilities);
        //Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://www.ykt.ru/");
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void TestHeader() throws Exception {
        String[] keys = {"дом", "home", "этиҥ", "2015"};
        for (String s : keys) {
            sendKey(s);
        }
    }


    private void sendKey(String word){
        WebElement query = driver.findElement(By.name("q"));
        query.clear();
        query.sendKeys(word);
        long end = System.currentTimeMillis() + 5000;
        while (System.currentTimeMillis() < end) {
            ArrayList<WebElement> resultsDiv = (ArrayList<WebElement>) driver.findElements(By.className("se_autocomplete--iconless"));
            if (resultsDiv.size()>0) {
                break;
            }
        }
        List<WebElement> allSuggestions = driver.findElements(By.xpath("//div[@class='se_autocomplete--iconless']"));
        for (WebElement suggestion : allSuggestions) {
            System.out.println(suggestion.getText());
        }
    }
}