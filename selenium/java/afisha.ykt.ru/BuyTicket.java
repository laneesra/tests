package afisha_ykt_ru;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;

import java.io.File;

public class BuyTicket {
    WebDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver, 10);

    @Before
    public void start() throws Exception{
        driver.get("http://afisha.ykt.ru/events/cinema");
    }

    @Test
    public void choosingSeat() throws Exception{
        WebElement item = driver.findElement(By.cssSelector(".seance-schedule_times_item"));
        Actions builder = new Actions(driver);
        builder.click(item).build().perform();
        try {
            WebElement seat = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".row-seats_seat.can-book.default")));
            seat.click();
            clickContinue();
        }
        catch (NoSuchElementException e){
            throw new NoSuchElementException("Нет свободных мест");
        }
    }

    public void clickContinue() throws Exception{
        try {
            WebElement next = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".event-action")));
            next.click();
        }
        catch (NoSuchElementException e){
            throw new NoSuchElementException("Кнопка 'Продолжить' недоступна");
        }
    }

    @After
    public void makeScreenshot() throws Exception {
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("screen.png"));
        driver.quit();
    }
}