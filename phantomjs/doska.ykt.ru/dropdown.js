var page = require('webpage').create();
var url = 'http://doska.ykt.ru/';
page.onConsoleMessage = function(msg) {
  console.log(msg);
};
page.open(url, function (status) {
  page.evaluate(function() {

    var $categoryDropdown = $('.d-sf_dropdown.category');
    var $categoryDropdownToggle = $categoryDropdown.find('.d-sf_dropdown_toggle');
    var $categoryDropdownMenu = $categoryDropdown.find('.d-sf_dropdown_menu');

    console.log('Дропдаун категории скрыт', $categoryDropdownMenu.css('display'), $categoryDropdownMenu.css('display') == 'none');

    $categoryDropdownToggle.trigger('click');
    setTimeout(function () {
      console.log('Дропдаун категории появился', $categoryDropdownMenu.css('display'), $categoryDropdownMenu.css('display') == 'block');
    }, 500);

  });

  setTimeout(function () {
    page.render('dropdown.png');
    phantom.exit();
  }, 500);
});
