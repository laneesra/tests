var page = require('webpage').create();
var url = 'http://doska.ykt.ru/post-add';
page.onConsoleMessage = function(msg) {
  console.log(msg);
};

page.open(url, function (status) {
  page.evaluateAsync(function() {
    var changeEvent = new Event('change'),
        cats = document.getElementById('cats'),
        subcats = document.getElementById('subcats'),
        rubrics = document.getElementById('rubrics'),
        queue = [cats, subcats, rubrics],
        i=0;

    function setField(obj) {
      obj.options[1].selected = true;
      obj.dispatchEvent(changeEvent);
      next();
    }

    function next(){
      setTimeout(function() {
        queue[i]  &&  setField(queue[i++]);
      }, 10);
    }
    next();
  });

  function outputResults(time){
    page.evaluateAsync(function() {
      var $smsField = $('.d-post-form_phone_raw > div').eq(1),
          $phoneVerified = $('.d-post-form_phone_verified');
      console.log('Поле кода активно', $smsField.css('display')=='block');
      console.log('Номер подтвержден', $phoneVerified.css('display')=='block', '\n');
    }, time);
  }

  function inputLetters(time){
    page.evaluateAsync(function() {
      console.log('Введен номер, содержащий недопустимые символы');
      $('#phone').trigger('focus').val('qwerty');
      $('.d-post-form_phone_col > button').eq(0).trigger('click');
    }, time);
  }

  function inputShortNumber(time){
    page.evaluateAsync(function() {
      console.log('Введен номер неправильный длины');
      $('#phone').trigger('focus').val('9999');
      $('.d-post-form_phone_col > button').eq(0).trigger('click');
    }, time);
  }

  function inputCorrectNumber(time){
    page.evaluateAsync(function() {
      console.log('Введен правильный номер');
      $('#phone').trigger('focus').val('1234567890');
      $('.d-post-form_phone_col > button').eq(0).trigger('click');
    }, time);
  }

  function inputConfirmCode(time){
    page.evaluateAsync(function() {
      console.log('Введен код подтверждения');
      $('#sms').trigger('focus').val('0000');
      $('.d-post-form_phone_col > button').eq(0).trigger('click');
    }, time);
  }

  inputLetters(100);
  outputResults(300);
  inputShortNumber(400);
  outputResults(500);
  inputCorrectNumber(600);
  outputResults(800);
  inputConfirmCode(900);
  outputResults(1100);


  setTimeout(function () {
    page.render('phone.png');
    phantom.exit();
  }, 1500);
});
