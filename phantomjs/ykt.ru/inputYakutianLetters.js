var page = require('webpage').create();
var url = 'http://ykt.ru/';

page.onConsoleMessage = function(msg) {
  console.log(msg);
};

page.open(url, function (status) {

  function inputYakutianLetters(){
    page.evaluateAsync(function() {
      var $letters = $('.letters > li');
      for (var i=0; i<$letters.length; i++){
        $letters.eq(i).trigger('click');
      }
      setTimeout(function () {
        console.log('Выведены все буквы:', $('#q-input').val()=="һөҕүҥ");
      }, 100);
    });
  }

  inputYakutianLetters();

  setTimeout(function () {
    page.render('main.png');
    phantom.exit();
  }, 1000);
});
